/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class UserInfo {
    int id;
    String username;
    String name;
    String surname;
    String password;
    String tel;
    int age;
    float weight;
    public UserInfo(int id,String username, String name, String surname,String password, String tel, int age, float weight){
        this.id = id;
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.tel = tel;
        this.age = age;
        this.weight = weight;
    }
    public int getId(){
        return id;
    }
    public String getUsername(){
        return username;
    }
    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
}
