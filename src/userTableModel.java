/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.table.*;
import java.util.*;
/**
 *
 * @author informatics
 */
public class userTableModel extends AbstractTableModel {
    String[] columnNames = {"id","username","name","surname"};
    ArrayList<UserInfo> userList = Data.userList;
    public userTableModel(){
        
    }
            
    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        return userList.size(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        UserInfo user = userList.get(rowIndex);
        if(user==null){
            return "";
        }
        switch(columnIndex){
            case 0: return user.getId();
            case 1: return user.getUsername();
            case 2: return user.getName();
            case 3: return user.getSurname();
        }
        return "";
    }
    
    
}
